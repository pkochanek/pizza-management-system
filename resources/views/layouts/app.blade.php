<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
    {{-- EMOJIS --}}
    <link href="https://afeld.github.io/emoji-css/emoji.css" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li><a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a></li>
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @guest
                @yield('content')
            @else

            <div class="container">
                <div class="row no-gutters">
                    {{-- Admin menu --}}
                    <div class="col-2">
                        <a href="{{ route('home') }}"><div class="menu-item">Dashboard</div></a>
                        <a href="{{ route('pizza.index') }}"><div class="menu-item">Pizzas</div></a>
                        <a href="{{ route('ingredients.index') }}"><div class="menu-item">Ingredients</div></a>
                        <a href="{{ route('side-dish.index') }}"><div class="menu-item">Side dishes</div></a>
                        <a href="{{ route('side-dish-type.index') }}"><div class="menu-item">Side dish types</div></a>
                        <a href="{{ route('pizza-sizes.index') }}"><div class="menu-item">Pizza sizes</div></a>
                        <a href="{{ route('categories.index') }}"><div class="menu-item">Categories</div></a>
                        <a href="{{ route('contact') }}"><div class="menu-item">Contact Info</div></a>
                        <a href="{{ route('admin') }}"><div class="menu-item">Change admin</div></a>
                    </div>
                    {{-- Admin body --}}
                    <div class="col-10">
                        @yield('admin-content')
                    </div>
                </div>
            </div>
                @endguest
        </main>
    </div>
</body>
</html>
